"""
This defines the thread in charge of running the stompclt
"""

import logging
import traceback
from subprocess import Popen, PIPE
import shlex
import signal
import time
import threading
import os


class StompClt(threading.Thread):
    """
    This thread is in charge of keeping running the stompclt

    :param cmd: command that starts the stompclt
    :param nb: number of stompclt to start
    """
    def __init__(self, cmd, id_, stop_flag):
        self._cmd = shlex.split(cmd)
        self._id = str(id_)
        self._stop_flag = stop_flag
        self._process = None
        threading.Thread.__init__(self, name="stompclt-" + self._id)
        self.setDaemon(True)

    def debug(self, msg):
        """ wrapper for logging """
        logging.debug("stompclt-" + self._id + " : " + msg)

    def error(self, msg):
        """ wrapper for logging """
        logging.error("stompclt-" + self._id + " : " + msg)

    def warning(self, msg):
        """ wrapper for logging """
        logging.warning("stompclt-" + self._id + " : " + msg)

    def critical(self, msg):
        """ wrapper for logging """
        logging.critical("stompclt-" + self._id + " : " + msg)

    def stop(self):
        """ Stop stompclt """
        if self._process and self._process.poll() is None:
            # check if the process is still running and send a kill signal
            self.debug("Sending a kill signal to pid " + \
                        str(self._process.pid))
            os.kill(self._process.pid, signal.SIGTERM)

    def run(self):
        """ Run the thread. """
        while not self._stop_flag.isSet():
            try:
                self._process = Popen(self._cmd, shell=False, stdout=PIPE,
                                      stderr=PIPE)
                # implicite wait
                stdout_, stderr_ = self._process.communicate()
            except Exception:
                self.critical("Critical error while trying to run" + \
                            "stompclt\n" + str(traceback.format_exc()))
                self._stop_flag.set()
                continue

            # if the command run produces an error
            if stderr_:
                self.warning("finished with stderr: " + stderr_)

            if self._process.returncode:
                self.error("error executing command: returncode " +\
                            str(self._process.returncode))
                # wait 30 seconds before trying again launching stompclt
                time.sleep(30)

        else:
            self.debug("stopped.")
            return

