"""
Module containing utilities for the Simple Log Producer
"""

# Imports
import sys
import os
import ConfigParser
import inspect
import logging


class ConfigError(Exception):
    """
    ConfigError
    :param msg:
    """
    def __init__(self, msg):
        Exception.__init__(self, msg)


def parse_config(filename):
    """
    Parse the configuration file and return a dictionnary

    :param filename: path to the config file

    :raises: ``RuntimeError`` if error while parsing the file
    :returns: a dictionnary of key-value configuration pairs
    """

    conf = dict()

    try:
        config = ConfigParser.ConfigParser()
        config.readfp(file(filename))
    except Exception, exc:
        raise RuntimeError('Error while parsing the config:' + str(exc))

    # Get general stuff
    # conf['log_file'] = config.get('Main', 'log_file')
    conf['plugin_path'] = config.get('Main', 'plugin_path')
    conf['chunk_size'] = int(config.get('Main', 'chunk_size'))

    # Get the files to monitor
    conf_files = config.get('Logs', 'files')
    conf['files'] = [ line.strip() for line in conf_files.split('\n') if line ]

    # Get the plugin list
    conf_plugins = config.get('Plugins', 'plugins')
    conf['plugins'] = [ tuple(line.split(' ')) \
                        for line in conf_plugins.split('\n') \
                        if line ]

    # Finally, get the misc stuff, which will be handled by the plugins...
    conf['misc'] = dict()
    if config.has_section('Misc'):
        for item in config.items('Misc'):
            conf['misc'][item[0]] = item[1]

    return conf


def redirect_logging(log_file_path):
    """ Set the logging to print to the log file """
    logging.basicConfig(
        filename=log_file_path,
        format='%(asctime)s:%(levelname)-8s  %(message)s',
        level=logging.DEBUG
    )


def create_plugin(module_name, class_name, config):
    """
    Create plugin of given class located in given module

    :param module_name: name of the module
    :param class_name: name of the class
    :param config: config dictionnary given to the plugin

    :returns: a instance of the plugin
    """
    if not module_name in sys.modules.keys():
        try:
            module = __import__(module_name)
        except ImportError, exc:
            raise ConfigError('Unable to load module: ' +\
                               module_name + ': ' + str(exc))
    else:
        module = sys.modules[module_name]

    classes = dict(inspect.getmembers(module, inspect.isclass))
    if not class_name in classes:
        raise ConfigError('Unable to find ' + \
                           class_name + ' class in ' + \
                           module_name )

    cls = classes[class_name]
    return cls(config)

