"""
Definition of the thread handling a log file.
"""

import os
import stat
import logging
import threading
import traceback
import time
try:
    import simplejson as json
except ImportError:
    import json


class LogFile(object):
    """
    LogFile class, a simple encapsulation of a file to shield the
    implementation details for handling file I/O.

    :param filepath: path to the log file
    """
    def __init__(self, filepath):
        self.inode    = -1
        self.file     = None
        self.lastread = time.time()
        self.filepath = filepath

    def __del__(self):
        """ Delete overload """
        self.close()

    def open(self):
        """ Open the file """
        try:
            self.file  = open(self.filepath, 'r')
        except:
            raise IOError
        else:
            self.inode = os.fstat(self.file.fileno())[stat.ST_INO]

    def close(self):
        """ Close the file for I/O """
        try:
            self.inode    = -1
            self.lastread = time.time()
            self.file.close()
        except AttributeError:
            pass  # Empty file object
        except IOError:
            pass  # File not open

    def closed(self):
        """
        Wrapper around the built-in file object's closed method. The method
        returns a bool indicating the current state of the file object.
        Additionally it returns True (closed) if the file object doesn't exist
        """
        try:
            return self.file.closed
        except AttributeError:
            return True  # Empty file object, consider as closed


class TailFile(LogFile):
    """
    TailFile class, a subclass of LogFile that provides support for handling
    log files under logrotation. A file is classified as being logrotated if
    A) The file has not been updated within the last 60 seconds
    B) A file with the same name but different filesystem inode exists

    The implementation is far from perfect and some logrotation options are
    not supported, e.g. copytruncate

    :param filepath: path to the log file
    :param caller: reference to the caller thread (only for logging purpose)
    """

    def __init__(self, filepath, caller):
        self._firstopen = True
        self._lastread  = time.time()
        self._caller = caller
        LogFile.__init__(self, filepath)

    def _rotated(self):
        """ Method returning a bool indicating if the file has been rotated """
        try:
            if time.time() - self._lastread < 60:
                return False  # The file is still consider as active
            return os.stat(self.filepath)[stat.ST_INO] != self.inode
        except OSError:
            return False  # Stat failure, consider as not rotated

    def readlines(self, sizehint=8192):
        """
        Wrapper around the readlines method of a file.
        Open the file if needed, check if rotated.
        """
        # Check if the file needs to be [re]opened
        if self.closed():
            try:
                self.open()
            except:
                raise IOError
                # return False
            else:
                self._caller.info("OPEN OK: " + self.filepath)

        if self._rotated():
            self._caller.info("ROTATED: " + self.filepath)
            self.close()
            try:
                self.open()
            except:
                raise IOError
                # return False
            else:
                self._caller.info("OPEN OK: " + self.filepath)

        # If this is the first time the log file has been opened, then seek to
        # the end. This avoids restarts sending duplicate information.
        if self._firstopen:
            self.file.seek(0, 2)  # SEEK_END
            self._firstopen = False

        return self.file.readlines(sizehint)


class FileMonitor(threading.Thread):
    """
    This thread is in charge of handling a file (parsing and publishing messages).
    An inifinite loop try to get new loglines, then hand them out to the chain of
    plugins, and finally publish the parsed result.

    :param thread_id: id for the thread
    :param file_to_monitor: path to the file the thread has to monitor
    :param conf: config dictionnary from main thread
    :param mq: message queue object (DQS)
    :param plugins: list of filter/parser plugins
    :pram stop_flag: shared stop flag
    """

    def __init__(self, thread_id, file_to_monitor, conf, plugins,
                 stop_flag, lock):
        self._thread_id = str(thread_id).zfill(2)
        self._file_to_monitor = file_to_monitor
        self._stop_flag = stop_flag
        self.chunk_size = conf['chunk_size']
        self.plugins = plugins
        # First open (for logging..)
        self._firstopen = True
        self.cutLine = None
        threading.Thread.__init__(self, name=self._thread_id)
        self.setDaemon(True)
        self.lock = lock

    def debug(self, injectorlogmessage):
        """ Wrapper method for debug logging """
        logging.debug("[" + self._thread_id + "]: " + injectorlogmessage)

    def info(self, injectorlogmessage):
        """ Wrapper method for info logging """
        logging.info("[" + self._thread_id + "]: " + injectorlogmessage)

    def error(self, injectorlogmessage):
        """ Wrapper method for error logging """
        logging.error("[" + self._thread_id + "]: " + injectorlogmessage)

    def _get_lines(self):
        """
        Try to read lines from the log file (the file will be open on the first read operation).
        If an exception is raised, this means that the file was not found. So sleep 60sec and
        loop to try again to open the file.

        Here we don't kill the threads that will loop forever, because we don't care.
        Indeed, if some log file are not very used, we might miss them if we kill these threads.
        So to keep it simple and silly, we let them loop forever...

        :returns: a list of log lines
        """
        lines = None
        try:
            lines = self._filehandler.readlines(sizehint=self.chunk_size)
        except IOError:
            if self._firstopen:
                self.info("_get_lines -- File not found : " +self._file_to_monitor)
                self._firstopen = False
            return None

        # If no lines, return
        if 0 >= len(lines):
            return None

        # Check if last line was cut because the buffer flush of the file writer don't write the end of the line before the python reader read it.
        # If it is the case, we will receive the first part of the string at the end of the list and the end part at the begining of next list of lines.

        # First, check if we have a cutLine to merge with the first of the list
        if self.cutLine is not None:
            self.debug("debugSLP - (get_lines merge part 1) cutLine = "+str(self.cutLine))
            self.debug("debugSLP - (get_lines merge part 2) will be added to "+str(lines[0]))
            lines[0] = self.cutLine + lines[0]
            self.debug("debugSLP - (get_lines merge part 3) result merge : "+str(lines[0]))
            self.cutLine = None

        # After, we have to check if there is an half string at the end of the list to put it in the next list of lines.
        if 0 < len(lines[-1]) and '\n' != lines[-1][-1]:
            self.debug("debugSLP - (get_lines check part 1) bad line finded : >>>"+str(lines[-1])+"<<<\n last char : >>>"+str(lines[-1][-1])+"<<<")
            self.cutLine = lines[-1]
            self.debug("debugSLP - (get_lines check part 2) new cutLine : >>>"+str(self.cutLine)+"<<<")
            self.debug("debugSLP - (get_lines check part 3) nb of lines in list before remove : "+ str(len(lines)))
            del lines[-1]
            self.debug("debugSLP - (get_lines check part 4) nb of lines in list after remove : "+ str(len(lines)))

        return lines


    def run(self):
        """ Simply run the thread. Loop forever."""
        self.info('== Starting thread... ==')
        self.info('File to open: ' + self._file_to_monitor)

        # Iniatilize TailFile object
        self._filehandler = TailFile(self._file_to_monitor, self)

        while not self._stop_flag.isSet():
            lines = self._get_lines()
            if not lines:
                # If we didn't get any data from the log file, take a nap to
                # prevent 100% CPU utilization.
                time.sleep(1)
                continue
            l = list() # we create a list of dict
            # Process the extracted lines.
            for line in lines:
                dic = dict()
                # Hand out the lines to the chain of plugins, which will
                # possibly parse them and build the result in a dict
                for plugin in self.plugins:
                    dic, line = plugin.parseline(dic, line)
                if dic:
                    l.append(dic)

            # Print the json dump
            if l:
                with self.lock:
                    for line in l:
                        print json.dumps(line)
        else:
            self.debug('stop flag set, exiting...')
            return


