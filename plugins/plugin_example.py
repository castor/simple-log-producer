class PluginExample(object):
    """
    Example of a plugin for the Simple Log Producer.
    
    The plugins must be in the 'plugin_path', specified in the section [Main] of the configuration
    file.
    They can get optional configuration data from the section [Misc] of the configuration file.

    The main program launches one thread for each log file. Then threads try to 'tail' their file.
    When they get a new line, they pass it to the chain of plugins, one by one.
    Eventually, after the last plugin of the chain, the line is empty, and the dic is full of
    key-value pairs.
    
    NB : this system of plugins was designed to parse little by little the lines of syslog files.
    But you can parse or filter whatever text-file you want, line by line. Or even more complex,
    correlate these data with external data. Nevertheless, keep in mind that these plugins are
    called against each new line, so performance might be an issue.

    :param config: the dictionnary composed of the key-value paired section [Misc] of the main 
                   config file
    """

    def __init__(self, config):
        print "plugin example init for instance : " + config['castor_instance']
        self.config = config

    def parseline(self, dic, line):
        """
        Function called by the main program.
        No order in the plugins is assumed, so all the plugins take a dict and a string as input,
        and return a dict and a string.
        Thus their role is to parse/filter the input string, put the result in the output dict,
        and put what they can't handle in the output string, hoping that the next plugin in the
        chain can handle it.

        :param dic: input dictionnary (empty for the first plugin)
        :param line: input string (should get smaller and smaller after each plugin)

        :return dic: output dictionnary (should grow after each plugin)
        :return line: output string (should slim down after each pluging, and eventually be empty
                      after the last plugin)
        """
        dic['castor_instance'] = self.config['castor_instance']
        return dic, line
