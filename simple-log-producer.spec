Summary: Simple log producer - Simple producer for AI messaging, tailing log files.
Name: simple-log-producer
Version: 0.6.0
Release: 0%{?dist}
Source: %{name}-%{version}.tar.gz
License: GPL
Group: Development/Libraries
BuildRoot: %{_builddir}/%{name}-%{version}-root
BuildArch: noarch
Url: http://castor.web.cern.ch

%if "%{?dist}" == ".slc6" || "%{?dist}" == ".el6"
%define simplejsondep python-simplejson
%else
# on CentOS7 (and presumably upper), the package has changed name
%define simplejsondep python2-simplejson
%endif

BuildRequires: python
Requires: %{simplejsondep}

%description
Log file is : /var/log/simple-log-producer.log

A good place to put the plugins is /etc/simple-log-producer/plugins

NB : simplejson is required.

%prep
%setup -n %{name}-%{version}

%define PYTHON_LIBDIR %(python -c "from distutils import sysconfig; print sysconfig.get_python_lib()")

%install
install -d $RPM_BUILD_ROOT/var/log -m 0755
install -d $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/slplib -m 0755
install -d $RPM_BUILD_ROOT/usr/bin -m 0755
install -d $RPM_BUILD_ROOT/etc/init.d -m 0755
install -d $RPM_BUILD_ROOT/etc/logrotate.d -m 0755
install -d $RPM_BUILD_ROOT/etc/simple-log-producer/plugins -m 0755
install -m 644 config.conf.EXAMPLE $RPM_BUILD_ROOT/etc/simple-log-producer
install -m 644 simple-log-producer.init $RPM_BUILD_ROOT/etc/init.d/simple-log-producer
install -m 644 simple-log-producer.logrotate $RPM_BUILD_ROOT/etc/logrotate.d/simple-log-producer
install -m 644 plugins/plugin_example.py $RPM_BUILD_ROOT/etc/simple-log-producer/plugins
install -m 755 simple-log-producer.py $RPM_BUILD_ROOT/usr/bin
install -m 644 slplib/__init__.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/slplib
install -m 644 slplib/file_monitor.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/slplib
install -m 644 slplib/stompclt.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/slplib
install -m 644 slplib/utils.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/slplib

%clean
%{__rm} -rf $RPM_BUILD_ROOT
%{__rm} -rf $RPM_BUILD_DIR/%{name}-%{version}

%files
%defattr(-,root,root,-)
%attr(0644,root,root) %doc /etc/simple-log-producer/config.conf.EXAMPLE
%attr(0644,root,root) /etc/simple-log-producer/plugins/plugin_example.py*
%attr(0644,root,root) /etc/logrotate.d/simple-log-producer
%attr(0775,root,root) /etc/init.d/simple-log-producer
%attr(0755,root,root) /usr/bin/simple-log-producer.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/slplib/__init__.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/slplib/file_monitor.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/slplib/stompclt.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/slplib/utils.py*


# %post
# /sbin/chkconfig --add simple-log-producer
# /sbin/service simple-log-producer condrestart > /dev/null 2>&1 || :
# %preun
# if [ "$1" = 0 ]; then
#     /sbin/service simple-log-producer stop > /dev/null 2>&1 || :
#     /sbin/chkconfig --del simple-log-producer
# fi


%changelog
* Tue Jun 13 2017 Giuseppe Lo Presti <giuseppe.lopresti@cern.ch> 0.6.0
- Cleaned up the default config file

* Mon Jan  9 2017 Giuseppe Lo Presti <giuseppe.lopresti@cern.ch> 0.5.0
- Refactored packaging with Makefile and a spec file
- Fixed dependency on CentOS 7

* Wed Aug 24 2016 Alexandre Terrien <alexandre.terrien@cern.ch 0.4.0-1
- Adaptation for new CASTOR monitoring infrastructure

* Thu Dec 17 2015 Sebastien Ponce <sebastien.ponce@cern.ch> 0.3.4-3
- Added missing BuildRequires for python, leading to broken CC7 packages

* Mon Dec 14 2015 Sebastien Ponce <sebastien.ponce@cern.ch> 0.3.4-2
- Dropped ownership of /var/log that breaks CC7

* Wed Nov 18 2015 Giuseppe Lo Presti <giuseppe.lopresti@cern.ch> 0.3.4-1
- Added log rotation script

* Fri Oct 02 2015 Sebastien Ponce <sebastien.ponce@cern.ch> 0.3.3-3
- 0.3.3-3 Avoid sending empty messages to stompclt (again)

* Thu Aug 20 2015 Sebastien Ponce <sebastien.ponce@cern.ch> 0.3.3-2
- 0.3.3-2 Avoid sending empty messages to stompclt

* Mon May 27 2013 Manuel Servais <manuel.servais@cern.ch> 0.3.3-1
- 0.3.3  refactoring + fix logfiles reading

* Mon May 27 2013 Manuel Servais <manuel.servais@cern.ch> 0.3.2-1
- 0.3.2  fix logfiles reading

* Mon May 27 2013 Manuel Servais <manuel.servais@cern.ch> 0.3.1-2
- 0.3.1-2  added post & pre install in spec file

* Mon May 27 2013 Manuel Servais <manuel.servais@cern.ch> 0.3.1-1
- 0.3.1 packaging + fix for logfiles reading

* Mon Jan 28 2013 Benjamin Fiorini <benjamin.fiorini@cern.ch> 0.3.0-1
- 0.3.0 packaging

* Fri Aug 10 2012 Benjamin Fiorini <benjamin.fiorini@cern.ch> 0.2.1-1
- 0.2.1 packaging

* Fri Aug 10 2012 Benjamin Fiorini <benjamin.fiorini@cern.ch> 0.2.0-1
- 0.2.0 packaging

* Mon Aug 06 2012 Benjamin Fiorini <benjamin.fiorini@cern.ch> 0.1.5-1
- 0.1.5 packaging

* Thu Jun 19 2012 Benjamin Fiorini <benjamin.fiorini@cern.ch> 0.1.4-1
- 0.1.4 packaging

* Wed May 30 2012 Benjamin Fiorini <benjamin.fiorini@cern.ch> 0.1.3-1
- 0.1.3 packaging

* Thu May 24 2012 Benjamin Fiorini <benjamin.fiorini@cern.ch> 0.1.2-1
- Initial packaging
