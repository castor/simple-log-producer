#!/usr/bin/python
#******************************************************************************
#                     simple-log-producer
#
# Copyright (C) 2011  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

"""
Simple Log producer tails a list of log files, give each line to a chain of
plugins. Each plugin tries to parse/filter the given line, puts the key-value
pairs in a dict, and passes the dict and the reduced line to the next plugin,
and so on.
Eventually we end up with an empty string and a dict containing all the wanted
key-value pairs.
Finally, the dict is dumpt in a Message object and sent to a directory queue.

It is designed to be a producer in the agile-infrastructure messaging system.

logging file       : /var/log/simple-log-producer.log
"""

# Imports

import sys
import traceback
import logging
import pprint
import signal
import threading
import time

from slplib import utils
from slplib.file_monitor import FileMonitor

# Configuration
pprint = pprint.PrettyPrinter(indent=4)

# Global variables
log_file_path = '/var/log/simple-log-producer.log'
STOP_FLAG = threading.Event()


# class AbstractPlugin(object):
#     """ This is an abstract class for the Plugin"""
#     def __init__(self, config):
#         pass
#     def parseline(self, dic=dict(), string=""):
#         return dict(), string
# cf plugins/plugin_example.py for more details about plugins


def exit_handler(signum=None, frame=None):
    """
    Handler assigned to the reception of SIGTERM and SIGINT signals

    :param signum:
    :param frame:
    """
    logging.debug("Main : got stop signal.")
    logging.info("Shutting down, could take a while...")
    STOP_FLAG.set()


def print_usage(script_name):
    """
    Simply print the usage help message.

    :param script_name: sys.argv[0], name of the calling script
    """
    sys.exit("Usage : python " + script_name + " /path/to/config/file.conf")


def main():
    """ Main """
    logging.info('=======================================')
    logging.info('==== Starting simple-log-producer =====')
    logging.info('=======================================')

    # Arguments handling
    try:
        conf_file = sys.argv[1]
    except IndexError:
        print_usage(sys.argv[0])
    logging.info("Starting with config file " + conf_file)

    # Parse the config
    try:
        conf = utils.parse_config(conf_file)
    except Exception:
        logging.error('Error parsing the configuration file ')
        logging.error(str(traceback.format_exc()))
        sys.exit()
    else:
        logging.debug('Starting with configuration : \n' + \
                      pprint.pformat(conf))

    # Instantiate every plugin
    if not conf['plugin_path'] in sys.path:
        sys.path.append(conf['plugin_path'])
    try:
        plugins = [ utils.create_plugin(plugin[0], plugin[1], conf['misc']) \
                    for plugin in conf['plugins'] ]
    except utils.ConfigError, exc:
        logging.critical(str(exc))
        sys.exit()

    # Start one thread per file to monitor
    lock = threading.Lock()
    for index, file_ in enumerate(conf['files']):
        # We use daemon thread so that waiting ones are instantaneously
        # stopped when exit is asked.
        FileMonitor(index, file_, conf, plugins, STOP_FLAG, lock).start()

    # Run until exit is asked
    # Here we need to wait "actively", and not wait() on the Event or join(),
    # cause the signals wouldn't be handled otherwise.
    while not STOP_FLAG.isSet():
        time.sleep(1)

    # Wait for active thread to terminate...
    time.sleep(2)

    # Then exit...
    sys.exit()


# Bootstrap
if __name__ == '__main__':
    # Redirect output to log file
    utils.redirect_logging(log_file_path)
    # Assign handler to signals
    signal.signal(signal.SIGINT, exit_handler)
    signal.signal(signal.SIGTERM, exit_handler)
    # Start main thread
    main()

